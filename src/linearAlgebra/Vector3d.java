//Leon Hazurin
//1640570
package linearAlgebra;
public class Vector3d {
private double x;
private double y;
private double z;

public Vector3d(double x, double y, double z) {
	this.x = x;
	this.y = y;
	this.z = z;
}

public double getX() {
	return this.x;
}
public double getY() {
	return this.y;
}
public double getZ() {
	return this.z;
}

public double magnitude() {
	return(Math.sqrt((x*x)+(y*y)+(z*z)));
}

public double dotProduct(Vector3d vector) {
	double product = ((this.x*vector.getX()) + (this.y*vector.getY()) + (this.z*vector.getZ()));
	return product;
}

public Vector3d add(Vector3d vector) {
	Vector3d newVector = new Vector3d((vector.getX()+this.x), (vector.getY()+this.y), (vector.getZ()+this.z));
	return newVector;
}

public boolean equals(Vector3d vector) {
	if ((vector.getX() == this.x) && (vector.getY() == this.y) && (vector.getZ() == this.z)) {
		return true;
	}
	else {
		return false;
	}
}
}
