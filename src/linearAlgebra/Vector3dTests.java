//Leon Hazurin
//1640570
package linearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGetters() {
		
		Vector3d vector = new Vector3d(1,2,3);
		assertEquals(1.0,vector.getX());
		assertEquals(2.0,vector.getY());
		assertEquals(3.0,vector.getZ());
	}
	
	@Test
	void testMagnitude() {
	Vector3d vector = new Vector3d(2,3,6);
	assertEquals(7.0,vector.magnitude());
	}
	
	@Test
	void testDotProduct() {
		Vector3d vector = new Vector3d(1,2,3);
		Vector3d vector2 = new Vector3d(2,3,6);
		double product = vector.dotProduct(vector2);
		assertEquals(26,product);
	}
	@Test
	void testAdd() {
		Vector3d vector = new Vector3d(1,2,3);
		Vector3d vector2 = new Vector3d(2,3,6);
		Vector3d vector3 = new Vector3d(3,5,9);
		assertTrue(vector3.equals(vector.add(vector2)));
	}
	}


